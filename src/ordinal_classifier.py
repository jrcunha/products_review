from sklearn.base import clone
from sklearn.base import BaseEstimator, ClassifierMixin
from sklearn.utils.validation import check_X_y, check_array, check_is_fitted
from sklearn.utils.multiclass import unique_labels
import numpy as np


class OrdinalClassifier(BaseEstimator, ClassifierMixin):

    def __init__(self, clfr=None, clf_type='xgb'):
        self.clfr = clfr
        self.classes_ = None
        self.clfrs = {}
        self.clf_type = clf_type
        self._params = clfr.get_params()
        print(self.clfr)

    def get_params(self, deep=True):
        return self._params

    def set_params(self, params):
        for key in self.clfr:
            self.clfr[key].set_params(**params)

    def fit(self, X_train, y_train):

        X_train, y_train = check_X_y(X_train, y_train)
        self.classes_ = unique_labels(y_train)

        for i in self.classes_[:-1]:
            labels = 1 * (y_train > i).astype(np.uint8)
            clfr = clone(self.clfr)

            weight_rate = np.sum(labels == 0) / np.sum(labels == 1)

            if self.clf_type == 'xgb':
                clfr.scale_pos_weight = weight_rate

            clfr.fit(X_train, labels)
            self.clfrs[i] = clfr

        print("Classes:", self.clfrs.keys())

    def _predict(self, X):

        check_is_fitted(self)
        X = check_array(X)

        preds_ = {i: self.clfrs[i].predict_proba(X)[:, 1] for i in self.classes_[:-1]}
        preds = []
        for y_class in self.classes_:
            if y_class == self.classes_[0]:
                preds.append(1 - preds_[y_class])
            elif y_class in self.classes_[:-1]:
                preds.append(preds_[y_class - 1] - preds_[y_class])
            else:
                preds.append(preds_[y_class - 1])
        return np.vstack(preds).T

    def predict(self, X):
        preds = self._predict(X)
        return np.array([self.classes_[i] for i in np.argmax(preds, axis=1)])

    def predict_proba(self, X):
        preds = self._predict(X)
        return np.max(preds, axis=1)
