import pandas as pd
import re
# from category_encoders import TargetEncoder
# import nltk
from unidecode import unidecode
from sklearn.naive_bayes import MultinomialNB
from sklearn.feature_extraction.text import CountVectorizer
import spacy
import spacy.lang.pt


class BuildFeature(object):
    def __init__(self):
        pass

    def get_target(self, df, label_name):
        target = df[label_name]
        return target

    def encode_categorical(self, df_, to_numeric_encode, label_name):
        encoder = TargetEncoder()
        df_[to_numeric_encode] = encoder.fit_transform(df_[to_numeric_encode], df_[label_name])

        return df_[to_numeric_encode]

    def clear_text(self, x):
        def remove_accentuation(x):
            return unidecode(x)

        x = x.strip().lower()
        tokenizer = nltk.RegexpTokenizer(r"\w+")
        x = tokenizer.tokenize(x)
        x = " ".join(x)
        x = remove_accentuation(x)
        return x

    def naive_bayes_encode(self, df, feature, label_name):
        X = df[feature].apply(lambda x: self.clear_text(x))
        y = df[label_name]

        vect = CountVectorizer().fit(X)
        X_vectorized = vect.transform(X)

        nbclf = MultinomialNB(alpha=0.1)
        nbclf.fit(X_vectorized, y)
        y_pred = nbclf.predict(X_vectorized)

        return y_pred

    def transform(self, df_):
        label_name = 'overall_rating'

        to_numeric_encode = ['product_name', 'product_id', 'product_brand',
                             'site_category_lv1', 'site_category_lv2',
                             'recommend_to_a_friend', 'reviewer_gender',
                             'reviewer_state']

        text_features = ['review_title', 'review_text']

        df = df_.copy()

        df[to_numeric_encode] = self.encode_categorical(df, to_numeric_encode, label_name)

        df['len_review_text'] = df['review_text'].apply(lambda x: len(x.split(" ")))
        df['len_review_title'] = df['review_title'].apply(lambda x: len(x.split(" ")))

        for col in text_features:
            df[col] = self.naive_bayes_encode(df, col, label_name)

        df['target'] = self.get_target(df, label_name)
        del df[label_name]

        final_columns = ['product_id',
                         # 'product_brand',
                         'site_category_lv1', 'site_category_lv2',
                         'recommend_to_a_friend',
                         'review_title', 'review_text',
                         'len_review_text', 'len_review_title',
                         'target']

        return df[final_columns], final_columns[:-1]


class Preprocess(object):

    def __init__(self):
        self.nlp = spacy.load("pt_core_news_lg")
        self.stop_words = spacy.lang.pt.STOP_WORDS

    def clear_text(self, text):
        """

        :param text:
        :return:
        """
        text = unidecode(text)
        text = re.sub(r"\W+", " ", text)
        text = re.sub("\d+", "", text)
        text = re.sub(" +", " ", text)

        return text.strip().lower()

    def normalize(self, text):
        """

        :param text:
        :return:
        """

        doc = self.nlp(text)
        text = " ".join([token.lemma_ for token in doc])
        return text

    def remove_stop_words(self, text):
        """

        :param text:
        :param stop_words:
        :return:
        """
        doc = self.nlp(text)

        text = " ".join([token.text for token in doc if not token is token.is_stop])

        return text

    def get_pos_tag(self, text):
        """

        :param text:
        :return:
        """
        doc = self.nlp(text)
        pos_tag = []

        for token in doc:
            pos_tag.append((token.text, token.pos_))

        return pos_tag

    def tranform(self, df, column):
        print("clearning text...")
        df[column] = df[column].apply(lambda x: self.clear_text(x))
        print("removing stop words...")
        df[column] = df[column].apply(lambda x: self.remove_stop_words(x))
        print("normalizing...")
        df[column] = df[column].apply(lambda x: self.normalize(x))

        return df
