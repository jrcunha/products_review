
### Visão geral

O problema de classificação de avaliações é diferente de um problema de classificação comum, pois o target (overall_rating)
é uma variável ordinal. Diferentemente de uma classifação nominal, em que todas as classes possuem o mesmo peso,
neste caso existe uma ordem na classe,de modo que uma avaliação igual a 5 é superior a outra igual 4, que é superior
3 e  assim por diante.

Ao tratar deste problema, como se fosse uma simples classificação, a informação da ordem das classes seriam perdidas.
Deste modo, decidi segui uma abordagem proposta neste artigo: https://www.cs.waikato.ac.nz/~eibe/pubs/ordinal_tech_report.pdf
que consiste numa forma de resolver problemas de classificação ordinal. 

Basicamente, o artigo propõe um tratamento adicional fazendo uso de um tipo de modelo de classificação qualquer, 
de modo que passamos a ter n-1 modelos de classificação binários, onde n é o número de classes do nosso problema.

Ao fim do treinamento dos modelos, faz-se um ranqueamento na predição de acordo com cada classe
e a partir daí chega-se na predição geral para um dado input.

Na solução deste problem, decidi testar um classificador linear (regressão logística) e outro não-linear 
(xgboost)  para fins de comparação.

A conclusão final é de que a abordagem do artigo não traz melhorias tão significativas em relação ao uso do xgboost como classificador
nominal, neste dataset. O xgboost apresentou um melhor resultado do que a regressão logistica.

### Requerements

Antes de executar os notebooks, execute instale os requirements do projeto:
```
$ pip install -r requirements.txt
```
### Notebooks

* A análise exploratória e entedimento da base de dados está em notebooks/notebook 1_EDA.ipynb
* O treinamento e avaliação dos modelos está no notebook notebooks/2_modeling_classification.ipynb
* O scripts de suporte, como preprocessamento, avaliação estão na pasta src
 